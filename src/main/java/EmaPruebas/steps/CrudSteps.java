package EmaPruebas.steps;

import org.jbehave.core.annotations.BeforeStories;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.MessageFormat;

public class CrudSteps {
    private static final Logger LOGGER = LoggerFactory.getLogger(CrudSteps.class);

    @BeforeStories
    protected final void beforeStories() {
        LOGGER.info(MessageFormat.format("{0} <<<<<< @BeforeStories", getClass().getCanonicalName()));
    }
}
