package EmaPruebas.steps;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Pending;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.ExpectedCondition;
import java.lang.*;
import java.util.*;
import org.openqa.selenium.support.ui.Select;
import java.net.URL;
import java.net.MalformedURLException;

public class MySteps {
    @Given("I am a pending step")
    public void givenIAmAPendingStep() throws InterruptedException {
        WebDriver driver;
        System.setProperty("webdriver.chrome.driver", "C:/Users/Ema/EmaPrueba1/src/main/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("http://www.google.com");

        // Find the text input element by its name
        WebElement element = driver.findElement(By.name("q"));

        // Enter something to search for
        element.sendKeys("Puto el que lee");
        Thread.sleep(100);

        // Now submit the form. WebDriver will find the form for us from the element
        element.submit();

        // Check the title of the page
        System.out.println("Page title is: " + driver.getTitle());

        // Google's search is rendered dynamically with JavaScript.
        // Wait for the page to load, timeout after 10 seconds
        (new WebDriverWait(driver, 30)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.getTitle().toLowerCase().startsWith("cheese!");
            }
        });

        // Should see: "cheese! - Google Search"
        System.out.println("Page title is: " + driver.getTitle());

        //Close the browser
        driver.quit();

    }

    //@Given("I am still pending step")
    @Pending
    public void givenIAmStillPendingStep() {
        // PENDING
    }

    @When("a good soul will implement me")
    @Pending
    public void whenAGoodSoulWillImplementMe() {
        // PENDING
    }

    @Then("I shall be happy")
    @Pending
    public void thenIShallBeHappy() {
        // PENDING
    }
    
}
